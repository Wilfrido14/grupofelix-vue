import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/style/jquery.fancybox.css'
import '@/assets/style/owl.carousel.min.css'
import '@/assets/style/owl.theme.default.min.css'
import '@/assets/style/owl.theme.green.min.css'
import '@/assets/style/style.css'
import '@/assets/style/responsive.css'

import '@/assets/js/jquery.fancybox.min.js'
import '@/assets/js/owl.carousel.min.js'
import '@/assets/js/main.js'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
