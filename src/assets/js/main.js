/* eslint-disable */
$(document).ready(function(){
  console.log('main')
  setTimeout(function(){
    $('#pre').removeClass('pre-loader');
    // $('#pre').css('display','none')
  }, 1000)
  setTimeout(function(){
    $('#pre').css('display','none')
  }, 1500)

  $('.carousel-principal-cars').owlCarousel({
    items: 1,
    autoplay:true,
    autoplayTimeout:10000,
    autoplayHoverPause: true,
    dots: false,
    nav:false,
    loop: true,
    margin:0,
    stagePadding: 0
  });

  $('.carousel-principal-testimonials').owlCarousel({
    items: 1,
    autoplay:true,
    autoplayTimeout:10000,
    autoplayHoverPause: true,
    loop: true,
    stagePadding: 0
  });


})


function setColorCar(id, color, route){
  /**
   * Por defecto si no se envía la ruta de la imagen se mostrará un coche en gris por defecto
   * Si la peticion cuenta con una raiz se localización se mostrará la imagen correcta
   */
  if(!route || route == null || route == undefined){
    $(`.${id}`).attr("src", `./resources/autos/default.png`);
  }
  else{
    $(`.${id}`).attr("src", `${route}${color}.png`);
  }
}


function showThisOptions(submenu, option)
{
  if(option)
  {
    $(`.${submenu}`).addClass("show");
  }
  else{
    $(`.${submenu}`).removeClass("show");
  }
}


document.querySelector('body').addEventListener("scroll", function (event) {
  if(this.scrollTop > 200)
  {
    $('#menu').addClass('fixed')
  }
  else{
    $('#menu').removeClass('fixed')
  }
});


$('.togglebutton').on('click', function() {
  $('.container-fluid').toggleClass('toggled');
})
